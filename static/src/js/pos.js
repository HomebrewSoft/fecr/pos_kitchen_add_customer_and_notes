odoo.define('pos_kitchen_add_customer_and_notes.pos', function (require) {
    "use strict";

    var screens = require('point_of_sale.screens');
    var core = require('web.core');
    var models = require("point_of_sale.models");

    var _t = core._t;

    models.load_fields("pos.order", [
        'client_simple',
    ]);

    var _super_order = models.Order.prototype;
    models.Order = models.Order.extend({
        export_as_JSON: function () {
            var data = _super_order.export_as_JSON.apply(this, arguments);
            data.client_simple = this.client_simple;
            return data;
        },
        init_from_JSON: function (json) {
            _super_order.init_from_JSON.call(this, json);
            this.client_simple = json.client_simple || '';
        },
    });

    var ClientSimpleButton = screens.ActionButtonWidget.extend({
        template: 'ClientSimpleButton',
        get_client_simple: function () {
            return (this.pos.get_order() && this.pos.get_order().client_simple) || 'Client Simple';
        },
        button_click: function () {
            var self = this;
            this.gui.show_popup('textinput', {
                'title': _t('Client Simple ?'),
                'value': this.pos.get_order().client_simple,
                'confirm': function (value) {
                    self.pos.get_order().client_simple = value;
                    self.renderElement();
                },
            });
        },
    });

    screens.define_action_button({
        'name': 'client_simple_action',
        'widget': ClientSimpleButton,
    });

    screens.OrderWidget.include({
        update_summary: function () {
            this._super();
            if (this.getParent().action_buttons && this.getParent().action_buttons.client_simple_action) {
                this.getParent().action_buttons.client_simple_action.renderElement();
            }
        },
    });
});

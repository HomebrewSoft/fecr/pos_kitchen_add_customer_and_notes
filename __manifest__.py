# -*- coding: utf-8 -*-
{
    'name': 'PoS Kitchen Add Customer and Notes',
    'version': '12.0.1.0.0',
    'author': 'HomebrewSoft',
    'website': 'https://homebrewsoft.dev/',
    'license': 'LGPL-3',
    'depends': [
        'aspl_pos_order_sync',
        'pos_kitchen_receipt_app',
    ],
    'data': [
        # security
        # data
        # templates
        'templates/assets.xml',
        # reports
        # views
        'views/pos_order.xml',
    ],
    'qweb': [
        'static/src/xml/client_simple_button.xml',
        'static/src/xml/pos_sale_ticket.xml',
        'static/src/xml/SaleNotelistLine.xml',
    ],
}

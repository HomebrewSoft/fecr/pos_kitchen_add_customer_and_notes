# -*- coding: utf-8 -*-
from odoo import _, api, fields, models


class PoSOrder(models.Model):
    _inherit = 'pos.order'

    client_simple = fields.Char(
    )

    @api.model
    def _order_fields(self, ui_order):
        res = super(PoSOrder, self)._order_fields(ui_order)
        res['client_simple'] = ui_order.get('client_simple')
        return res
